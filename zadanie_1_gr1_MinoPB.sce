// Author : __MinoPB__   
// environment : SCILAB 
// Date of creation: 2019-10-16
// Have a nice day sir (or madam (: )!


function[] = Calculator()
//ex. 1
h = 6.62607015E-34  //Planck's number value
Ans1 = h / 2*%pi    //Answer's value
// Ans1 gives h*%pi/2 instead of h/(2*%pi)   :(

//ex. 2
Ans2 = sin((%pi/6) / exp(1))  //Answer's value


//ex. 3
hex = hex2dec('00123d3')    //convert hexadecimal number to decimal system for further calculations
Ans3 = hex / 2.455E23   //Answer's value


//ex. 4
Ans4 = sqrt(exp(1)- %pi) //Answer's value - it's complex number.


//ex. 5
format (12)    //incrase showing numbers after coma
a = %pi * 1E10   
b = floor(a)   //multiplying Pi by 1E10 and round it, to have its 10th number after the coma as natural number
b/10           //dividing the number over 10 to get 1 number after coma  
floor(ans)     //rounding the number again 
d = ans*10     //multiplying to get number in same size as "b"

Ans5 = b-d     //answer's value. The result is Pi's 10th number after coma


//ex. 6
date0 = clock()  //number value of actual date
dateb = [1997 08 22 13 13 13.1300]  //date of my birthday 

ddate = etime(date0,dateb)  //difference betwen actual date and may birthday in [s]
Ans6 = ddate/86400    // answer in days


//ex. 7
Rad = 6371 //Earth's Radius in [Km]
Log = log(Rad/1E5)             // Some bieger elements of equation 
Sq =  sqrt(7)/2                // just for minimalization chance of error
Hexi = hex2dec('aabb')         

Ans7 = atan(exp(1)^(Sq - Log)/Hexi) //Answer's value


//ex. 8
Avo = 6.02214076E23   //avogadro's number wich is number of molecules in 1 mole of substantion

Ans8 = (Avo * 10E-6) /5    // Answer's value


//ex. 9 
// Ethanol => C2H5OH, ergo, ethanol unit mass is 46 [u] 
// 2C12 unit mass is 24 [u]

C12 = 24/46 *1000  //promil C12 in Ethanol 
//because C13 is occursed 100 times less than C12 ->
Ans9 = (C12/100)    //Answer's value


endfunction

exec(Calculator, 7)  //execute whole function whith "echo" and print prompts after forcing by "enter"
